// C++ Test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <chrono>
#include <thread>


LARGE_INTEGER frequency;        // ticks per second
LARGE_INTEGER t1, t2;           // ticks
double elapsedTime;

//Append datastructures
const unsigned int inserts = 250000;
DoubleLinkedList doubleLinkedList;
std::vector<int> vectorList;
std::vector<int> vectorListWReserve;
std::deque<int> dequeList;
std::queue<int> queueList;
std::stack<int> stackList;
int* basicArray = new int[inserts];

//Prepend datastructures
DoubleLinkedList doubleLinkedList2;
std::vector<int> vectorList2;
std::vector<int> vectorListWReserve2;
std::deque<int> dequeList2;
std::queue<int> queueList2;
std::stack<int> stackList2;
int* basicArray2 = new int[inserts];

int main()
{
	vectorListWReserve.reserve(inserts);
	vectorListWReserve2.reserve(inserts);

	std::cout << "Append:" << std::endl;

	//DoubleLinkedList Append

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	for (size_t i = 0; i < inserts; i++)
	{
		doubleLinkedList.Append(i);
	}


	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	std::cout << "DoubleLinkedList: \t" << elapsedTime << "ms" << std::endl;

	//Basic Array Append
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);


	for (int i = 0; i < inserts; i++)
	{
		basicArray[i] = i;
	}

	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	std::cout << "Basic Array: \t\t" << elapsedTime << "ms" << std::endl;

	//VectorList Append

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	for (size_t i = 0; i < inserts; i++)
	{
		vectorList.push_back(i);
	}


	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	std::cout << "Vector: \t\t" << elapsedTime << "ms" << std::endl;

	//VectorList with reserve Append

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	for (size_t i = 0; i < inserts; i++)
	{
		vectorListWReserve.push_back(i);
	}


	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	std::cout << "Vector with reserve: \t" << elapsedTime << "ms" << std::endl;

	//Deque Append

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	for (size_t i = 0; i < inserts; i++)
	{
		dequeList.push_back(i);
	}


	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	std::cout << "Deque: \t\t\t" << elapsedTime << "ms" << std::endl;

	//Queue Append

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	for (size_t i = 0; i < inserts; i++)
	{
		queueList.push(i);
	}


	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	std::cout << "Queue: \t\t\t" << elapsedTime << "ms" << std::endl;

	//Stack Append

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	for (size_t i = 0; i < inserts; i++)
	{
		stackList.push(i);
	}


	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	std::cout << "Stack: \t\t\t" << elapsedTime << "ms" << std::endl;



	std::cout << "Prepend:" << std::endl;

	//DoubleLinkedList Prepend

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	for (size_t i = 0; i < inserts; i++)
	{
		doubleLinkedList2.Prepend(i);
	}


	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	std::cout << "DoubleLinkedList: \t" << elapsedTime << "ms" << std::endl;

	//Basic Array Prepend
	int N = 0;
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	for (int i = 0; i < inserts; i++)
	{
		memcpy(basicArray2 + 1, basicArray2, N * sizeof(int));
		basicArray2[0] = i;
		N++;
	}

	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	std::cout << "Basic Array: \t\t" << elapsedTime << "ms" << std::endl;
	
	//VectorList Prepend

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	for (size_t i = 0; i < inserts; i++)
	{
		vectorList2.insert(vectorList2.begin(), i);
	}


	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	std::cout << "Vector: \t\t" << elapsedTime << "ms" << std::endl;

	//VectorList with reserve Prepend

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	for (size_t i = 0; i < inserts; i++)
	{
		vectorListWReserve2.insert(vectorListWReserve2.begin(), i);
	}


	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	std::cout << "Vector with reserve: \t" << elapsedTime << "ms" << std::endl;

	//Deque Prepend

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	for (size_t i = 0; i < inserts; i++)
	{
		dequeList2.push_front(i);
	}


	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	std::cout << "Deque: \t\t\t" << elapsedTime << "ms" << std::endl;

	//Queue Prepend
	std::cout << "Queue: \t\t\t" << "N/A" << std::endl;

	//Stack Prepend
	std::cout << "Stack: \t\t\t" << "N/A"  << std::endl;

	delete[] basicArray;
	delete[] basicArray2;

	return 0;
}

