#pragma once

struct Node
{
	Node* next;
	Node* prev;
	int data;

	Node(int value) : data(value)
	{
		next = nullptr;
		prev = nullptr;
	};
};

class DoubleLinkedList
{
public:

	DoubleLinkedList()
	{
		_head = new Node(0);
		_tail = new Node(0);

		_head->next = _tail;
		_tail->prev = _head;
	};

	~DoubleLinkedList()
	{
		Node* currentNode = _head;
		Node* next = nullptr;

		while (currentNode != nullptr)
		{
			next = currentNode->next;
			delete currentNode;
			currentNode = next;
		}
	};

	void Append(int value)
	{
		_tail->next = new Node(value);
		_tail->next->prev = _tail;
		_tail = _tail->next;
	};

	void Prepend(int value)
	{
		_head->prev = new Node(value);
		_head->prev->next = _head;
		_head = _head->prev;
	}

private:
	Node * _head;
	Node* _tail;
};

